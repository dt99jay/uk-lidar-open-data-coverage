UK LiDAR Open Data Coverage
---------------------------
The LiDAR data published by Environment Agency is a terrific resource, but doesn't cover the whole of England. It's diffcult to tell with confidence[^map] whether coverage exists until you have downloaded and unzipped the appropriate zip file.

The files in this repository might be useful if you need to programatically work out whether there is coverage for a particular location before downloading the LiDAR data itself (if you don't need to do it programatically, you can see coverage [here](https://dt99jay.cartodb.com/viz/fef16d9c-b39c-11e5-b0d3-0e31c9be1b51/public_map) or [the 1m DSM here](https://houseprices.io/lab/lidar/map)).

The repository includes:

* Shapefiles for the footprint of each 10km square's coverage
* Shapefile and GeoJSON for England, merged from the above
* A CSV list of each 1km square, including:
    * Grid reference of parent 10km square
    * ASCII file name of 1km square (inc. grid ref)
    * Count of 'no data' (i.e. -9999) measurements in file
    * Indication of partial or complete data[^nodata]
    * File size in bytes

Currently these files are generated from the composite 2m DTM data (the DSM should have the same footprint), as of January 2016.

See a [map of the coverage on CartoDB](https://dt99jay.cartodb.com/viz/fef16d9c-b39c-11e5-b0d3-0e31c9be1b51/public_map).

The Shapefiles were generated with GDAL/OGR and Python. The Shapefile and GeoJSON for England have been simplified[^simp] to make them a more manageable size.

**To Do**

* Add footprint of 1m composite data from Environment Agency
* Add 2m and 1m footprints for Wales from Natural Resources Wales

**Credits**

* LiDAR data covering England is © Environment Agency copyright and/or database right 2015. All rights reserved.
* Christopher Gutteridge's [catalogue of LiDAR data](https://github.com/cgutteridge/uklidar) was used to download each zip file
* OS grid used on the map [made available by Charles Roper](https://github.com/charlesroper/OSGB_Grids)

[^map]:The map on [data.gov.uk](http://environment.data.gov.uk/ds/survey) shows which 1km squares have some coverage, but doesn't indicate whether this coverage is partial or complete. For example, the Isle of Wight is shown as complete, because some data exists for each 1km square, but there are actually at least five gaps in coverage.

[^nodata]: This flag shouldn't be relied on. A file is marked as 'partial' if there are more than 1 instances of -9999 found in the ASCII file (there is always one in the header), but there are reasons why a file marked as 'partial' might be complete e.g. if it crosses an area of coastline and the 'no data' measurements are over the sea.

[^simp]: The file was simplified with OGR's tolerance set to 100. Also, the raw data has many small areas of measurements over the sea (e.g. off Workington in Cumbria as well as other areas) – these have been removed to keep the file size down.